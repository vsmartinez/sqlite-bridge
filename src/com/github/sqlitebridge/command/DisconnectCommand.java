package com.github.sqlitebridge.command;

import com.github.sqlitebridge.Context;

public class DisconnectCommand implements Command {

	private static final String EXIT_COMMAND = ".disconnect";

	@Override
	public void execute(Context context) {
		context.stop();
	}

	@Override
	public boolean applyTo(String str) {
		return str != null && str.equalsIgnoreCase(EXIT_COMMAND);
	}

	@Override
	public String getDescription() {
		return "Disconnect client";
	}

	@Override
	public String getCommandName() {
		return EXIT_COMMAND;
	}
}
