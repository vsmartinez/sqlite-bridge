package com.github.sqlitebridge.command;

import com.github.sqlitebridge.Context;

public interface Command {
	
	public String getCommandName();
	
	public boolean applyTo(String str);

	public String getDescription();

	public void execute(Context context);
}
