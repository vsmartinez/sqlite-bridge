package com.github.sqlitebridge.formatter;

import java.io.IOException;
import java.io.OutputStream;

public interface Formatter<T> {

	public void writeError(String error, OutputStream out) throws IOException;

	public void write(T cursor, OutputStream out) throws IOException;

	public void writeOk(OutputStream out) throws IOException;

}
