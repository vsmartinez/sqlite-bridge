package com.github.sqlitebridge.formatter;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Writer;

import android.database.Cursor;
import android.database.SQLException;

public abstract class CursorStreamFormatter implements Formatter<Cursor> {
	protected static final String NEW_LINE = "\n";

	@Override
	public void writeError(String error, OutputStream out) throws IOException {
		if (out == null) {
			throw new RuntimeException("Output stream can't be null");
		}

		PrintWriter writer = new PrintWriter(out);
		writer.write("error");
		writer.write(NEW_LINE);
		writer.write(error);
		writer.write(NEW_LINE);
		writer.write(-1);
		writer.flush();
	}

	@Override
	public void writeOk(OutputStream out) throws IOException {
		if (out == null) {
			throw new RuntimeException("Output stream can't be null");
		}

		PrintWriter writer = new PrintWriter(out);
		writer.write("ok");
		writer.write(NEW_LINE);
		writer.write(-1);
		writer.flush();
	}

	private void writeResult(PrintWriter writer) throws IOException {
		// write overall result
		writer.write("ok");
		writer.write(NEW_LINE);
		writer.flush();
	}

	private String toTypeName(int type) {
		if (type == Cursor.FIELD_TYPE_BLOB) {
			return "BLOB";
		}

		if (type == Cursor.FIELD_TYPE_FLOAT) {
			return "FLOAT";
		}

		if (type == Cursor.FIELD_TYPE_INTEGER) {
			return "INT";
		}

		if (type == Cursor.FIELD_TYPE_NULL) {
			return "NULL";
		}

		if (type == Cursor.FIELD_TYPE_STRING) {
			return "STRING";
		}

		return "NULL";
	}

	private String[] getTypes(Writer writer, Cursor cursor) {
		int columns = cursor.getColumnCount();
		String[] types = new String[columns];
		for (int i = 0; i < columns; i++) {
			types[i] = toTypeName(cursor.getType(i));
		}
		return types;
	}

	protected abstract void writeHeader(Writer writer, String... headers)
			throws IOException;

	// protected abstract void writeTypes(Writer writer, String... types) throws
	// IOException;

	protected abstract void writeBody(Writer writer, Cursor cursor)
			throws IOException;

	@Override
	public void write(Cursor cursor, OutputStream out) throws IOException {

		if (out == null) {
			throw new RuntimeException("Output stream can't be null");
		}

		PrintWriter writer = new PrintWriter(out);

		try {
			// write overall result
			writeResult(writer);

			// // write type name of each column
			// writeTypes(writer, getTypes(writer, cursor));

			// write column name
			writeHeader(writer, cursor.getColumnNames());

			// write body
			writeBody(writer, cursor);

			// write end of stream
			writer.write(-1);

			// flushing to the server
			writer.flush();
		} catch (SQLException e) {
			writeError(e.getMessage(), out);
		}
	}

}
