package com.github.sqlitebridge.formatter;

import java.io.IOException;
import java.io.Writer;

import android.database.Cursor;

public class CsvFormatter extends CursorStreamFormatter {

	private static final String SEPARATOR = ",";

	public CsvFormatter() {
	}

	@Override
	protected void writeHeader(Writer writer, String... names) throws IOException {
		for (int i = 0; i < names.length; i++) {
			writer.write(names[i]);
			if (i != names.length - 1) {
				writer.write(SEPARATOR);
			}
		}

		writer.write(NEW_LINE);
		writer.flush();
	}

	@Override
	public void writeBody(Writer writer, Cursor cursor) throws IOException {
		// write data
		int column = cursor.getColumnCount();
		while (cursor.moveToNext()) {
			for (int i = 0; i < column; i++) {
				String name = cursor.getString(i);
				writer.write(name == null ? "null" : name);
				if (i != column - 1) {
					writer.write(SEPARATOR);
				}
			}
			writer.write(NEW_LINE);
			writer.flush();
		}
	}
}
