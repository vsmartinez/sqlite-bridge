package com.github.sqlitebridge;


public interface Context {
	
	void start();
	
	void stop();

}
