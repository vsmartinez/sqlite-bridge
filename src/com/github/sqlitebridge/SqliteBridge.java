package com.github.sqlitebridge;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import android.database.Cursor;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.github.sqlitebridge.command.Command;
import com.github.sqlitebridge.command.DisconnectCommand;
import com.github.sqlitebridge.formatter.CsvFormatter;
import com.github.sqlitebridge.formatter.Formatter;

public class SqliteBridge {

	private static final int PORT = 20004;
	private static final String TAG = "SqlBridge";
	private static final String SELECT_STATEMENT = "select";
	private static final String INSERT_STATEMENT = "insert";
	private static final String UPDATE_STATEMENT = "update";
	private static final String DELETE_STATEMENT = "delete";
	private static final String COMMAND_PREFIX = ".";

	private static final int MAX_CONNECTIONS = 10;

	private Thread serverWorker = null;
	private ExecutorService threadPool;

	private ServerSocket server = null;

	private Formatter<Cursor> formatter = null;

	private Command defaultCommand = null;

	private SQLiteDelegator delegator = null;

	public SqliteBridge(final SQLiteDelegator delegator) {
		this(delegator, null);
	}

	public SqliteBridge(final SQLiteDelegator delegator,
			final Command defaultCommand) {
		this.delegator = delegator;

		threadPool = Executors.newFixedThreadPool(MAX_CONNECTIONS);

		// Csv default formatter
		this.formatter = new CsvFormatter();

		// Default command
		this.defaultCommand = defaultCommand;

		// Adding commands
		addCommand(new DisconnectCommand());

	}

	public SqliteBridge(final SQLiteOpenHelper db) {
		this(db, null);
	}

	public SqliteBridge(final SQLiteOpenHelper db, final Command defaultCommand) {
		this(new SQLiteDelegator() {

			@Override
			public Cursor rawQuery(String sql) {
				return db.getReadableDatabase().rawQuery(sql, null);
			}

			@Override
			public void execSQL(String sql) {
				db.getWritableDatabase().execSQL(sql);

			}
		}, defaultCommand);
	}

	public void start() {

		serverWorker = new Thread(new Runnable() {

			@Override
			public void run() {
				startServer();
			}
		});

		serverWorker.start();
	}

	private void startServer() {

		try {

			Log.d(TAG, "Starting server...");
			server = new ServerSocket(PORT);

			while (server != null) {

				Log.d(TAG, "Waiting...");
				Socket socket = server.accept();

				Log.d(TAG, "Accepted connection : " + socket);

				threadPool.submit(new ClientWorker(socket));
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (server != null) {
					server.close();
				}
			} catch (IOException e) {
				// Nothing to do here
			}
		}

	}

	private class ClientWorker implements Runnable, Context {
		private boolean running = false;

		private Socket socket;

		ClientWorker(Socket client) {
			this.socket = client;
		}

		@Override
		public void run() {

			try {
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(socket.getInputStream()));

				running = true;

				while (running) {

					String input = read(reader);

					if (input == null || input.trim().length() == 0) {
						continue;
					}

					// Logging input statement/command
					Log.d(TAG, ">" + input);

					String tmp = input.trim().toLowerCase(Locale.getDefault());

					if (isCommand(tmp)) {
						processCommand(tmp, this);
						// Due is a command, not action should be taken in
						// Statement section below
						continue;
					}

					try {
						// Process SELECT statement
						if (tmp.startsWith(SELECT_STATEMENT)) {
							executeRawStatement(input, socket.getOutputStream());
							continue;
						}

						// Process DELETE statement
						if (tmp.startsWith(DELETE_STATEMENT)) {
							executeStatement(input, socket.getOutputStream());
							continue;
						}

						// Process UPDATE statement
						if (tmp.startsWith(UPDATE_STATEMENT)) {
							executeStatement(input, socket.getOutputStream());
							continue;
						}

						// Process INSERT statement
						if (tmp.startsWith(INSERT_STATEMENT)) {
							executeStatement(input, socket.getOutputStream());
							continue;
						}

						// Process any other statement which doesn't match with
						// above. ie. explain query plan
						executeRawStatement(input, socket.getOutputStream());
					} catch (RuntimeException e) {
						e.printStackTrace();
						// Send back to the client the error
						formatter.writeError(e.getMessage(),
								socket.getOutputStream());
					}
				}

				socket.close();
				System.out.println("Client worker stopped.");
			} catch (IOException e) {
				e.printStackTrace();
			}

		}

		@Override
		public void start() {
			// Nothing to do here
		}

		@Override
		public void stop() {
			running = false;
			System.out.println("Stopping client worker.");
		}
	}

	private String read(Reader reader) throws IOException {
		int ch = -1;
		StringBuffer sb = new StringBuffer();

		while ((ch = reader.read()) != -1) {
			if (ch == 65535) {
				return sb.toString();
			} else {
				sb.append((char) ch);
			}
		}

		return null;
	}

	private boolean isCommand(String command) {
		return command != null && command.trim().startsWith(COMMAND_PREFIX);
	}

	private void processCommand(String command, Context context) {
		Command c = commands.get(command);
		if (c != null) {
			c.execute(context);
			return;
		}

		if (defaultCommand != null) {
			defaultCommand.execute(context);
		}
	}

	private Map<String, Command> commands = new HashMap<String, Command>();

	public void addCommand(Command command) {
		if (command == null) {
			return;
		}

		commands.put(command.getCommandName(), command);
	}

	public void removeCommand(Command command) {
		if (command == null) {
			return;
		}

		commands.remove(command);
	}

	public void setDefaultCommand(Command command) {
		defaultCommand = command;
	}

	public void setFormatter(Formatter<Cursor> formatter) {
		this.formatter = formatter;
	}

	private void executeStatement(String sql, OutputStream out)
			throws IOException {
		delegator.execSQL(sql);
		formatter.writeOk(out);
	}

	private void executeRawStatement(String sql, OutputStream out)
			throws IOException {

		Cursor cursor = null;

		try {
			cursor = delegator.rawQuery(sql);
			formatter.write(cursor, out);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}
	}

	public void stop() {

		if (threadPool != null) {
			try {
				threadPool.shutdownNow();
			} catch (SecurityException e) {

			}
		}

		threadPool = null;

		serverWorker.interrupt();
	}

	public static interface SQLiteDelegator {
		Cursor rawQuery(String sql);

		void execSQL(String sql);
	}
}
