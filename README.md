# README #

sqlite-brige creates a server socket to listen incoming SQL statements from [sqlite-bridge-client](https://bitbucket.org/vsmartinez/sqlite-bridge-client) and run it against your database instance in your Android project.


### How to use it? ###
* You need to create a jar file with compiled classes and import it into [sqlite-bridge-example](https://bitbucket.org/vsmartinez/sqlite-bridge-example)

# License #
```
Copyright 2015 Vidal Santiago Martinez. vidalsantiagomartinez at gmail.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```